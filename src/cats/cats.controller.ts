import { Controller, Get, Req, Post, Delete, ForbiddenException } from '@nestjs/common';

@Controller('cats')
export class CatsController {
    @Post()
    create(): string {
        return 'This action created a new cat';
    }

    @Get()
    findAll(): string {
        return 'This action returns all cats';
    }

    @Delete()
    delete(): string {
        throw new ForbiddenException('cannot kill cats!!');
    }
}
